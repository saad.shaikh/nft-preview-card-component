# Title: NFT preview card component

Tech stack: Vue.js and Vite

Deployed project: https://saad-shaikh-nft-preview-card-component.netlify.app/


## Desktop view:
![Desktop view](design/desktop-design.jpg) 

## Mobile view:
![Mobile view](design/mobile-design.jpg) 
